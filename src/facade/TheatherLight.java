package facade;

public class TheatherLight {

    private static TheatherLight theatherLight = new TheatherLight();

    public static TheatherLight getTheatherLight() {
        return theatherLight;
    }

    public void on(){
        System.out.println("theatherLight on");
    }

    public void off(){
        System.out.println("theatherLight off");
    }

    public void dim(){
        System.out.println("theatherLight dimm..");
    }

    public void bright(){
        System.out.println("theatherLight bright");
    }
}
