package facade;

public class HomeTheaterFacade {

    //定义各个子系统的对象

    private TheatherLight theatherLight;
    private Popcorn popcorn;
    private Screen screen;
    private Projector projector;
    private Stereo stereo;
    private DVDPlayer dvdPlayer;

    HomeTheaterFacade(){
        this.theatherLight = TheatherLight.getTheatherLight();
        this.popcorn = Popcorn.getPopcorn();
        this.screen = Screen.getScreen();
        this.projector = Projector.getProjector();
        this.stereo = Stereo.getStereo();
        this.dvdPlayer = DVDPlayer.getDvdPlayer();
    }

    //操作分为4步走

    public void ready(){
        popcorn.on();
        popcorn.off();
        screen.down();
        projector.on();

        stereo.on();
        dvdPlayer.on();
        theatherLight.dim();
    }

    public void play(){
        dvdPlayer.play();

    }

    public void pause(){
        dvdPlayer.pause();
    }

    public void end(){
        popcorn.off();
        theatherLight.bright();
        screen.up();
        projector.off();
        stereo.off();
        dvdPlayer.off();
    }

    public static void main(String[] args) {

    }
}
