package facade;

public class DVDPlayer {

    private static DVDPlayer dvdPlayer = new DVDPlayer();

    public static DVDPlayer getDvdPlayer() {
        return dvdPlayer;
    }

    public void on(){
        System.out.println("DVD on");
    }

    public void off(){
        System.out.println("DVD off");
    }

    public void play(){
        System.out.println("DVD is Playing");
    }

    public void pause(){
        System.out.println("DVD is pause");
    }
}
