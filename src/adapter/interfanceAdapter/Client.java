package adapter.interfanceAdapter;

public class Client {
    public static void main(String[] args) {
        AbsAdapter absAdapter = new AbsAdapter(){
            @Override
            public void m1(){
                System.out.println("重写了M1方法");
            };
        };

        absAdapter.m1();
    }
}
