package adapter.ObjectAdapter;

public class VoltageAdapter implements IVoltage5V {
    Voltage22V voltage22V;

    VoltageAdapter(Voltage22V voltage22V){
        this.voltage22V = voltage22V;
    }

    public int output5v() {
        int a = 0;
        if(voltage22V!=null){
            int src = voltage22V.outPut220V();
            return src/44;
        }else {
            return a;
        }
    }
}
