package adapter.classAdapter;

public class VoltageAdapter extends Voltage22V implements IVoltage5V{
    @Override
    public int output5v() {
        int src = super.outPut220V();
        return src/44;
    }
}
