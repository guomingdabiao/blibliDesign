package protoType;

public class Clint {
    public static void main(String[] args) throws Exception{
//        Sheep sheep = new Sheep("苏西","2");
//
//        Sheep sheep1 = (Sheep) sheep.clone();
//
//        System.out.println("sheep1"+sheep1);


           DeepProtoType deepProtoType =  new DeepProtoType();
           deepProtoType.name = "测试工具人";
           deepProtoType.deepCloneableTarget = new DeepCloneableTarget("工具人2号","工具人类");
//方式1完成深拷贝
//           DeepProtoType deepProtoType1 = (DeepProtoType) deepProtoType.clone();
//           System.out.println("deepProtoType.name"+ deepProtoType.name+"deepProtoType.deepCloneableTarget.hashCode="+deepProtoType.deepCloneableTarget.hashCode());
//
//           System.out.println("deepProtoType1.name"+ deepProtoType1.name+"deepProtoType1.deepCloneableTarget.hashCode="+deepProtoType1.deepCloneableTarget.hashCode());
//

        //方式2完成深拷贝
        DeepProtoType deepProtoType1 = (DeepProtoType) deepProtoType.DeepClone();
        System.out.println("deepProtoType.name"+ deepProtoType.name+"deepProtoType.deepCloneableTarget.hashCode="+deepProtoType.deepCloneableTarget.hashCode());

        System.out.println("deepProtoType1.name"+ deepProtoType1.name+"deepProtoType1.deepCloneableTarget.hashCode="+deepProtoType1.deepCloneableTarget.hashCode());

    }
}
