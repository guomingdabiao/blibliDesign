package protoType;

public class Sheep implements Cloneable{
    private String name;
    private String age;

    Sheep(String name,String age){
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    protected Object clone()  {
        try {
            return super.clone();
        }catch (Exception e){
            e.getMessage();
        }
        return null;
    }

    @Override
    public String toString() {
        return "name: "+name+" age: "+age;
    }
}
