package bridge;

public class FoldedPhone extends Phone{


    FoldedPhone(Brand brand) {
        super(brand);
    }

    void open(){
        System.out.println("折叠手机");
        super.open();
    };
    void close(){
        System.out.println("折叠手机");
        super.close();
    };
    void call(){
        System.out.println("折叠手机");
        super.call();
    };
}
