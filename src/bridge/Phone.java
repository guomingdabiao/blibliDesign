package bridge;

public abstract class Phone {
    Brand brand;

    Phone(Brand brand){
        this.brand = brand;
    }

    void open(){
        brand.open();
    };
    void close(){
       brand.close();
    };
    void call(){
        brand.call();
    };
}
