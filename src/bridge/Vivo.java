package bridge;

public class Vivo implements Brand{
    @Override
    public void open() {
        System.out.println("Vivo打开");
    }

    @Override
    public void close() {
        System.out.println("Vivo关闭");
    }

    @Override
    public void call() {
        System.out.println("Vivo信息");
    }
}
