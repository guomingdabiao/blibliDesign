package bridge;

import java.sql.Driver;

public class Client {

    public static void main(String[] args) {
        Phone phone = new FoldedPhone(new Vivo());
        phone.open();
        phone.call();
        phone.close();


    }
}
