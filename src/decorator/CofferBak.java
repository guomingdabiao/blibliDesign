package decorator;

import sun.rmi.runtime.Log;

public class CofferBak {

    public static void main(String[] args) {
        //装饰者模式下的2份牛奶加一份豆浆的LongBlack

        Drink drink = new LongBlack();
        System.out.println("单点一份LongBlank的价格是"+drink.getPrice());

        drink = new Milk(drink);
        System.out.println("一份LongBlank加一份牛奶的价格是"+drink.cost());
        System.out.println(drink.getDes());


        drink = new Milk(drink);
        System.out.println("一份LongBlank加两份牛奶的价格是"+drink.cost());

        drink = new Soy(drink);
        System.out.println("一份LongBlank加两份牛奶和一份豆浆的价格是"+drink.cost());


    }
}
