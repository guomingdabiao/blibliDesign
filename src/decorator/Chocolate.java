package decorator;

//具体的Decorator
public class Chocolate extends Decorator{
    public Chocolate(Drink drink) {
        super(drink);
        super.setDes("巧克力");
        super.setPrice(2.0f);
    }
}
