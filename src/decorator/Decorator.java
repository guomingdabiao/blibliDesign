package decorator;

public class Decorator extends Drink{
    private Drink drink;

    public Decorator(Drink drink){  //组合
        this.drink = drink;
    }
    @Override
    public float cost() {
        //super.getPrice方法是获取自己的价格
        return super.getPrice()+drink.getPrice();
    }

    @Override
    public String getDes() {
//        return super.getDes()+drink.getDes();
        //drink.getDes是被装饰者的信息
        return des+" "+super.getPrice()+drink.getDes();
    }
}
