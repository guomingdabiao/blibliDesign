package decorator;

public class Milk extends Decorator{
    public Milk(Drink drink) {
        super(drink);
        super.setDes("牛奶");
        super.setPrice(22.0f);
    }
}
