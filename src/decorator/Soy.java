package decorator;

public class Soy extends Decorator{
    public Soy(Drink drink) {
        super(drink);
        super.setDes("豆浆");
        super.setPrice(1.0f);
    }
}
