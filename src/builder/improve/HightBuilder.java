package builder.improve;

public class HightBuilder implements HouseBuilder{
    @Override
    public void builderBasiz() {
        System.out.println("高楼打地基100米");
    }

    @Override
    public void builderWalls() {
        System.out.println("高楼砌墙1000米");

    }

    @Override
    public void roofed() {
        System.out.println("高楼封顶");
    }
}
