package builder.improve;

public class Client {

    public static void main(String[] args) {

        StringBuilder stringBuilder = new StringBuilder("Hello World");


        //准备盖普通房子
        HouseBuilder houseBuilder = new commonHouse();

        //创建房子的建造者
        HouseDirector houseDirector = new HouseDirector(houseBuilder);

        House house = houseDirector.construcHouse();


        //高层
        houseBuilder = new HightBuilder();
        houseDirector.setHouseBuilder(houseBuilder);
        House house1 = houseDirector.construcHouse();


    }
}
