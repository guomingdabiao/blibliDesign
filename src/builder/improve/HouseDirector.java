package builder.improve;

public class HouseDirector {

    HouseBuilder houseBuilder = null;

    //通过构造器传入HuouseBuilder
    public HouseDirector(HouseBuilder houseBuilder){
        this.houseBuilder = houseBuilder;
    }

    //通过Setter方法传入

    public void setHouseBuilder(HouseBuilder houseBuilder){
        this.houseBuilder = houseBuilder;
    }

    //如何建造房子,交给建造者
    public House construcHouse(){
        houseBuilder.builderBasiz();
        houseBuilder.builderWalls();
        houseBuilder.roofed();
        return houseBuilder.builderHouse();
    }

}
