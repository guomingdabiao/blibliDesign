package builder.improve;

public class commonHouse implements HouseBuilder {
    @Override
    public void builderBasiz() {
        System.out.println("普通房子打地基10米");
    }

    @Override
    public void builderWalls() {
        System.out.println("普通房子砌墙100米");
    }

    @Override
    public void roofed() {
        System.out.println("普通房子封顶");
    }
}
