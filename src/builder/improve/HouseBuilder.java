package builder.improve;

//抽象的建造者
public interface HouseBuilder {

    House house = new House();

    //将建筑的流程写好,抽象的方法
    public void builderBasiz();

    public void builderWalls();

    public void roofed();

    //建造房子
    default public House builderHouse(){
        return house;
    }
}
