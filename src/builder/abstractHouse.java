package builder;

public abstract class abstractHouse {

    //打地基
    public abstract void builderBisac();

    //砌墙
    public abstract void builderWalls();

    //封顶
    public abstract void roofed();

    public void builder(){
        this.builderBisac();
        this.builderWalls();
        this.roofed();
    }

}
