package compoite;

import java.util.ArrayList;
import java.util.List;

public class College extends OrganizationComponent{

    List<OrganizationComponent> list = new ArrayList<>();

    College(String name, String describe) {
        super(name, describe);
    }

    @Override
    public void add(OrganizationComponent organizationComponent) {
        this.list.add(organizationComponent);
    }

    @Override
    public void remove(OrganizationComponent organizationComponent) {
        this.list.remove(organizationComponent);
    }

    @Override
    public void println() {
        System.out.println("~~~~~~~~~~~~~~~~"+super.getName()+"~~~~~~~~~~~~~~~~");

        list.forEach(t->{
            t.println();
        });
    }
}
