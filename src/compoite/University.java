package compoite;

import java.util.ArrayList;
import java.util.List;

public class University extends OrganizationComponent{
    List<OrganizationComponent> list = new ArrayList<>();
    University(String name, String describe) {
        super(name, describe);
    }

    @Override
    public void add(OrganizationComponent organizationComponent) {
        list.add(organizationComponent);
    }

    @Override
    public void remove(OrganizationComponent organizationComponent) {
        list.remove(organizationComponent);
    }

    @Override
    public void println() {
        System.out.println("~~~~~~~~~~~~~~"+super.getName()+"~~~~~~~~~~~~~~~~~~");

        list.forEach(t->{
            t.println();
        });
    }
}
