package compoite;

public class Client {

    public static void main(String[] args) {
        OrganizationComponent organizationComponent = new University("江南大学","测试的");

        OrganizationComponent organizationComponent1 = new College("食品工程","食品工程");
        OrganizationComponent organizationComponent2 = new College("物联网","物联网");


        organizationComponent1.add(new Department("食堂工程1","食堂工程1"));
        organizationComponent1.add(new Department("食堂工程2","食堂工程2"));
        organizationComponent2.add(new Department("物联网1","物联网1"));
        organizationComponent2.add(new Department("物联网2","物联网2"));


        //将学院放入学校
        organizationComponent.add(organizationComponent1);
        organizationComponent.add(organizationComponent2);

        organizationComponent.println();


    }
}
