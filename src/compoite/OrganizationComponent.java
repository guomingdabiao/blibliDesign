package compoite;

public abstract class OrganizationComponent {

    private String name;
    private String describe;

    OrganizationComponent(String name,String describe){
        this.name = name;
        this.describe = describe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public abstract void println();

    public void add(OrganizationComponent organizationComponent){

    }

    public void remove(OrganizationComponent organizationComponent){

    }

}
