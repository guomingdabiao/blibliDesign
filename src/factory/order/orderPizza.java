package factory.order;

import factory.enumUtils.PizzaType;
import factory.pizza.Pizza;
import factory.pizza.aPizza;
import factory.pizza.bPizza;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class orderPizza {
    Pizza pizza = null;
    String name;

    orderPizza(){
        getPizza();
    }

    public void getPizza(){
        for (;;){
            name = getPizasName();
            if("a".equals(name)){
                pizza = new aPizza();
            }else if("b".equals(name)) {
                pizza = new bPizza();
            }else {
                System.out.println("错误的披萨");
                break;
            }
            pizza.setName(PizzaType.getName(name));
            pizza.prepare();
            pizza.pake();
            pizza.cut();
            pizza.box();
        }
    }

    String getPizasName(){
        BufferedReader strin = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("少年,请选择你要吃的披萨：");
        String name = null;
        try {
            name = strin.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return name;
    }
}
