package factory.pizza;

public abstract class Pizza {
    private String name; //披萨名称

    public abstract void prepare(); //准备过程

    public void pake(){  //烘烤
        System.out.println(name+ " paking");
    }

    public void cut(){  //切割
        System.out.println(name+ " cutting" );
    }

    public void box(){ //打包
        System.out.println(name +" boxing");
    }

    public void setName(String name) {
        this.name = name;
    }
}
