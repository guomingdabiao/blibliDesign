package factory.enumUtils;

public enum PizzaType {
    APIZZA("A披萨","a"),
    BPIZZA("B披萨","b");

    private String name;
    private String type;

    PizzaType(String name,String type){
        this.name = name;
        this.type = type;
    }

//    public String getName(){
//        return name;
//    }

    public static String getName(String type){
        for (PizzaType value : PizzaType.values()) {
            if(type.equals(value.type)){
                return value.name;
            }
        }
        return null;
    }


}
