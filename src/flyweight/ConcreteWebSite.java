package flyweight;

public class ConcreteWebSite extends WebSite{

    private String type;

    //构造器
    ConcreteWebSite(String type){
        this.type = type;
    }
    @Override
    public void use() {
        System.out.println("网站的发布形式为: "+type);
    }
}
